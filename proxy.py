#!/usr/env/bin python3

import threading
import socket
import select
import os


SOCKS_VERSION = 5



class Proxy:
	def __init__(self, username, password):
		self.username = username
		self.password = password


	def handleClient(self, connection):
		# greeting header
		# read and unpack 2 bytes from a client
		version, nmethods = connection.recv(2)

		# get available methods [0, 1, 2]
		methods = self.getAvailableMethods(nmethods, connection)

		# accept only USERNAME/PASSWORD auth
		if 2 not in set(methods):
			# close connection
			connection.close()
			return

		# send welcome message
		connection.sendall(bytes([SOCKS_VERSION, 2]))

		if not self.verifyCredentials(connection):
			return

		# request (version=5)
		version, command, _, addressType = connection.recv(4)

		if addressType == 1:  # IPv4
			address = socket.inet_ntoa(connection.recv(4))
		elif addressType == 3:  # Domain name
			domainLength = connection.recv(1)[0]
			address = connection.recv(domainLength)
			address = socket.gethostbyname(address)

		# convert bytes to unsigned short array
		port = int.from_bytes(connection.recv(2), 'big', signed=False)

		try:
			if command == 1:  # CONNECT
				remote = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
				remote.connect((address, port))
				bindAddress = remote.getsockname()
			else:
				connection.close()

			addr = int.from_bytes(socket.inet_aton(bindAddress[0]), 'big', signed=False)
			port = bindAddress[1]

			reply = b''.join([
				SOCKS_VERSION.to_bytes(1, 'big'),
				int(0).to_bytes(1, 'big'),
				int(0).to_bytes(1, 'big'),
				int(1).to_bytes(1, 'big'),
				addr.to_bytes(4, 'big'),
				port.to_bytes(2, 'big')
			])
		except Exception as e:
			# return connection refused error
			reply = self.generateFaildReply(addressType, 5)

		connection.sendall(reply)

		# establish data exchange
		if reply[1] == 0 and command == 1:
			self.exchangeLoop(connection, remote)

		connection.close()

	
	def exchangeLoop(self, client, remote):
		while True:
			# wait until client or remote is available for read
			r, w, e = select.select([client, remote], [], [])

			if client in r:
				data = client.recv(4096)
				if remote.send(data) <= 0:
					break

			if remote in r:
				data = remote.recv(4096)
				if client.send(data) <= 0:
					break

	
	def generateFaildReply(self, addressType, errorNumber):
		return b''.join([
			SOCKS_VERSION.to_bytes(1, 'big'),
			errorNumber.to_bytes(1, 'big'),
			int(0).to_bytes(1, 'big'),
			addressType.to_bytes(1, 'big'),
			int(0).to_bytes(4, 'big'),
			int(0).to_bytes(4, 'big')
		])


	def verifyCredentials(self, connection):
		version = ord(connection.recv(1)) # should be 1

		usernameLength = ord(connection.recv(1))
		username = connection.recv(usernameLength).decode('utf-8')

		passwordLength = ord(connection.recv(1))
		password = connection.recv(passwordLength).decode('utf-8')

		if username == self.username and password == self.password:
			# success, status = 0
			response = bytes([version, 0])
			connection.sendall(response)
			return True

		# failure, status != 0
		response = bytes([version, 0xFF])
		connection.sendall(response)
		connection.close()
		return False


	def getAvailableMethods(self, nmethods, connection):
		methods = []
		for i in range(nmethods):
			methods.append(ord(connection.recv(1)))
		return methods

	def run(self, host, port):
		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		s.bind((host, port))
		s.listen()

		while True:
			conn, addr = s.accept()
			t = threading.Thread(target=self.handleClient, args=(conn,))
			t.start()


if __name__ == "__main__":
	USERNAME = os.environ["USERNAME"]
	PASSWORD = os.environ["PASSWORD"]
	
	proxy = Proxy(USERNAME, PASSWORD)
	proxy.run("0.0.0.0", 8080)
